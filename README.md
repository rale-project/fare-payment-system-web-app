# QR Code Fare Payment System Web App

The QR Code Fare Payment System offers commuters a hassle-free way to manage their transportation expenses by leveraging the convenience of QR code technology and mobile connectivity. Here's how it works: when customers purchase their personalized QR code cards from designated booth sellers, they register their card with their mobile phone number.

Once registered, any top-up made at the booth sellers is instantly reflected in the customer's account associated with their registered phone number. This seamless integration ensures that commuters can easily monitor and manage their card balance directly from their smartphones.

When boarding public transportation, passengers simply present their QR code cards at entry points and scan them using their smartphones or dedicated scanners. The fare is deducted from the updated balance linked to their registered phone number, making the entire process smooth and efficient.

By providing real-time updates to customers' mobile phones, the QR Code Fare Payment System offers greater transparency and convenience. Commuters can easily track their spending, receive alerts for low balances, and seamlessly reload their cards as needed, all from the palm of their hand.

For transport providers, this system streamlines revenue collection and enhances operational efficiency, as all transactions are digitally recorded and updated in real-time. Overall, the QR Code Fare Payment System represents a modern and user-friendly approach to public transportation ticketing, making commuting easier and more accessible for everyone.

[QR Code Fare Payment System Web App](https://gitlab.com/rale-project/fare-payment-system-web-app/-/tree/master)

[Card image sample](https://gitlab.com/rale-project/fare-payment-system/-/blob/main/card-design-front.jpg)

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/rale-project/fare-payment-system-web-app.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/rale-project/fare-payment-system-web-app/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
## Author
Ralph Ledesma
